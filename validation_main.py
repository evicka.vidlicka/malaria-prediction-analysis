# This script performs ML model validation by analysing its results.
# If the input data is incorrect or missing, the script will stop 
# and display an error message. If you have any questions or feedback, 
# please contact me at eva.vidlickova@gmail.com.

import pandas as pd
import validation_library as lib

# the considered year
year = 2021
cleaning = False
analysing = True
plotting = False
# .txt file containing list of names of all validation data files for
# the considered year

if cleaning:
    # relative path to the folder with all validation data for the considered year 
    data_path = '.validation_data/dirty_data/'
    # one of the four data types/formats
    for data_type in ['malaria_per_sheet', 'week_per_column', \
         'malaria_per_column', 'week_per_sheet']:
        # create list of file names and store it in a text file ()
        all_file = lib.create_file_names(data_path + data_type + '/', f'2021_{data_type}.txt', True)

        # iterate over all xlsx files in the specified folder
        for i in range(len(all_file)):
            print(i)
            print('Region: ', all_file[i])
            # extract needed data (number of malaria cases and serious malaria cases)
            # from file 'i' stored in folder 'data_folder'
            cases = lib.extract_and_transform(year, data_path, data_type, all_file[i])
            cases = lib.uniformize_name(cases)
            
            # storing 
            cases.to_csv('./validation_data/clean_data/' +\
                all_file[i].rstrip('.xlsx')+'.csv')

if analysing:
    # create list of file names and store it in a text file (for the future)
    all_file = lib.create_file_names('../1_Processed_Data/2021/clean_data/',
         f'2021_all.txt', True)

    # to store data from all districts, for overall analysis
    all_data = []
    # iterate over all clean data
    for i in range(len(all_file)):
        cases = pd.read_csv('../1_Processed_Data/2021/clean_data/' +\
            all_file[i])
        # create merged tables with both validation data and computed data
        merged_data = lib.merge_data(cases, year, all_file[i])

        # perform the analysis
        tmp = lib.validate_data(merged_data, all_file[i])
        if tmp is not None:
            all_data.append(tmp)

        # create plots
        if plotting:
            lib.plot_validation_data(merged_data, all_file[i])
    
    # analysis and plotting of data for all districts combined
    lib.overall_stats_and_plots(pd.concat(all_data), True)



