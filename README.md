This project performs a validation of the performance of an ML model used to predict malaria outbreaks in Burkina Faso. Its results, in the forms of confidence intervals, are compared with raw field data.

The input data (both raw and computed) is sensitive, therefore it cannot be provided here. In case of interest, contact me for me information at eva.vidlickova@gmail.com.

The libraries to be installed are specified in requirements.txt.

To run the script:

python3 validation_main.py

The script performs 3 operations: data cleaning, data analysis and data visualization.

The cleaning script takes data from './dirty_data' to './clean_data', all written in the same format, with different file for different district.

The analysis district then reads the clean data, matches them with computed data, compares
whether the confidence intervals managed to capture the true malaria cases and prints results
in './results/statistics/', with different info per district. Information on unmatched data is
available in './results/corrupted_info/', with name of csps and as reason.

The plotting stores plots for each district and each csps unit, stored in './results/png_files/'.

