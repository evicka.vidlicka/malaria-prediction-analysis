import numpy as np
import pandas as pd
import unidecode
import matplotlib.pyplot as plt
import os

def create_file_names(data_folder, output_name = "2021_all_filenames.txt", 
        storing = False):
    """
    Create a list containing the names of all .xlsx files in a folder 
    specified by a relative path given in 'data_folder'.

    If storing is set to True, the output is stored in a .txt file 
    specified in output_name, (defaultly "2021_all_filenames.txt") with the name of each .xlsx file
    on a different line.
    """
    list_filenames = []
    if storing:
        if output_name == None:
            raise ValueError('You forgot to provide the output-file name for storing!')
        a = open(output_name, "w")
    # iterate over all files in the folder
    for path, subdirs, files in os.walk(data_folder, 'r'):
        for filename in files:
            # the 'TLOH 2021-KV_BON.csv' is broken
            if filename != 'TLOH 2021-KV_BON.csv' and filename.endswith(".csv"):
                if storing:
                    a.write(filename + os.linesep)
                list_filenames.append(filename)
    return list_filenames

def uniformize_name(data):
    """
    Taking care of certain typical mistakes in csps names.
    """
    # set all letters lower-case
    data['csps'] = data['csps'].map(lambda x: x.lower())
    # set all letters to be unidecode
    data['csps'] = data['csps'].map(
        lambda x: unidecode.unidecode(x))
    # remove all spacing in the front and back
    data['csps'] = data['csps'].map(lambda x: x.strip())
    # replace space in names by underscores
    data['csps'] = data['csps'].map(lambda x: x.replace(' ', '_'))
    return data

def extract_and_transform(year, data_path, data_type, file_name):
    """
    Return an excel-like table with index as csps name, column as week,
    values as number of malaria cases.
    """
    excel_file = data_path + data_type + '/' + file_name
    # check for which type of format the current file is
    if data_type == 'week_per_sheet':
        # append data from all 52 weeks into a list of tables
        frames = []
        for week in range(52):
            tmp_data = pd.read_excel(excel_file, sheet_name=week, 
                header=None, usecols=[0,1])
            tmp_data[2] = week + 1
            frames.append(tmp_data)

        # create one table from the list of tables corresponding to each week
        validation_data = pd.concat(frames)
        # rename columns
        validation_data.columns = ['csps', 'cases', 'week']
        # specify the year
        validation_data['year'] = year
        # grouping based on malaria cases
        grouped_cases = pd.pivot_table(validation_data, values='cases', \
            index='csps', columns='week')
        # make index into first columns
        grouped_cases = grouped_cases.reset_index(level=0)

    elif data_type == 'malaria_per_column':
        # read csps name, week, number of malaria cases
        validation_data = pd.read_excel(excel_file,
                header=None, usecols=[0,1,2])
        # provide column names
        validation_data.columns = ['csps', 'week', 'cases']
        validation_data['week'] = validation_data['week'].map(lambda x: int(np.floor(x)))
        validation_data['week'] = validation_data['week'].astype(int)
        # add year
        validation_data['year'] = year
        # grouping based on malaria cases
        grouped_cases = pd.pivot_table(validation_data, values='cases', \
            index='csps', columns='week')
        # make index into first columns
        grouped_cases = grouped_cases.reset_index(level=0)
    
    elif data_type == 'malaria_per_sheet':
        grouped_cases = pd.read_excel(excel_file, sheet_name=0, 
                header=None)
        grouped_cases.rename(columns={0: 'csps'}, inplace=True)
    
    elif data_type == 'week_per_column':
        # read columns 0,1,4,7,10,13...
        cols = [0] + [3*a + 1 for a in range(52)]
        grouped_cases = pd.read_excel(excel_file, header=None,
            skiprows = [0], usecols=cols)
        grouped_cases.columns = ['csps'] + list(range(1,53))

    else:
        raise ValueError('Folder name not recognized!')

    return grouped_cases

def merge_data(cases, year, region_name='This region', verbose = False):
    """
    Create tables containing computed and validation data for further analysis.
    This script merges computed data and validation data only for the units
    where we have all this data available.
    """
    region_name = region_name.rstrip('.xlsx')
    print('')
    print('Current district: ', region_name)
    cases['username'] = 0
    # Set the column names to be strings if integers, representing week numbers
    cases.rename(columns={col: int(np.floor(float(col))) for col in cases.columns[2:-1]}, inplace=True)
    cases.rename(columns={col: str(col) for col in cases.columns}, inplace=True)

    with open('./results/statistics/' + region_name.rstrip('.csv') + '.txt', 'w') as f:
        f.write(f'Number of all csps : {len(cases)}. \n')

    # read csps info to identify every csps' name with its username_integer
    csps_info = pd.read_csv('../../Forecaster-Data/early_warning_malaria_csps_info.csv', 
        usecols = ['username', 'mobile_username_integer'])

    current_csps = cases['csps']
    for i in range(cases.shape[0]):
        # check if csps exists in the database csps_info, otherwise
        # no computed results for this unit
        try:
            cases.at[i,'username'] = \
                int(csps_info.loc[csps_info['username'] == \
                    current_csps[i], 'mobile_username_integer'].item())
        except:
            cases.at[i,'username'] = 0

    corrupted_data = cases.loc[cases['username'] == 0, 'csps'].to_frame()
    corrupted_data['reason'] = 'Not found in csps_info database.'
    print('There is', len(corrupted_data), 'unidentified units out of',\
        len(cases),', which include: ')
    if verbose:
        print(corrupted_data['csps'].to_list())

    computed_data = pd.read_csv('../../Forecaster-Data/early_warning_malaria.csv', 
        usecols = ['mobile_username_integer', 'weekdate', 'two_tailed_68_lower',
        'two_tailed_68_upper', 'two_tailed_95_lower', 'two_tailed_95_upper'])
    computed_data['weekdate'] = pd.to_datetime(computed_data['weekdate'])
    computed_data['year'] = computed_data.weekdate.apply(lambda x: x.year)
    computed_data['week_of_year'] = computed_data.weekdate.apply(lambda x: x.weekofyear)
    computed_data['mobile_username_integer'] = computed_data['mobile_username_integer'].map(
         lambda x: int(x))

    available = np.where(cases['username'].to_numpy() != 0)[0]

    available_results = []
    not_computed = []
    for i in range(np.size(available)):
        test_index = available[i]
        test_username_itg = cases.loc[test_index, 'username']
        test_weeks = computed_data.loc[
            (computed_data['mobile_username_integer']==test_username_itg) & \
            (computed_data['year']==year), 'week_of_year'].to_numpy()
        if test_weeks.size == 0:
            not_computed.append(str(cases.loc[test_index, 'csps']))
            if verbose:
                print('No computed data for this csps: '+ \
                    str(cases.loc[test_index, 'csps']).capitalize())
        else:
            try:
                values = computed_data.loc[
                    (computed_data['mobile_username_integer']==test_username_itg) & \
                    (computed_data['year']==2021) & \
                    (computed_data['week_of_year'].isin(test_weeks)), ['two_tailed_68_lower',
                    'two_tailed_68_upper', 'two_tailed_95_lower', 'two_tailed_95_upper']]
                values['true_cases'] = cases.loc[cases['username']==test_username_itg, \
                    [str(x) for x in test_weeks]].to_numpy()[0]
                values['csps'] = str(cases.loc[test_index, 'csps']).capitalize()
                values.index = test_weeks
                available_results.append(values)
            except:
                print('Did not match computed data and field data for some reason.')
    if len(not_computed) > 0:
        tmp_df = pd.DataFrame(not_computed, columns=['csps'])
        tmp_df['reason'] = 'Not found computed results for prediction.'
        corrupted_data = pd.concat([corrupted_data, tmp_df])
    corrupted_data.index = np.arange(0,len(corrupted_data))
    corrupted_data.to_csv('./results/corrupted_info/' + region_name)

    return available_results

def validate_data(available_results, region_name='This region', verbose = True):
    """
    Validate prediction results based on comparing computed confidence
    intervals and true data from field.
    """
    if len(available_results) == 0:
        print('No data to validate')
        return None
    else:
        if verbose:
            print('Number of csps with available info: ', len(available_results))
        with open('./results/statistics/' + region_name.rstrip('.csv') + '.txt', 'a') as f:
            f.write(f'Number of csps with available info: {len(available_results)}. \n')
        
    data = pd.concat(available_results)
    
    # overall statistics for current region
    data['two_tailed_68_valid'] = np.where((data['two_tailed_68_lower'] < data['true_cases']) \
        & (data['two_tailed_68_upper'] > data['true_cases']), True, False)

    data['two_tailed_95_valid'] = np.where((data['two_tailed_95_lower'] < data['true_cases']) \
        & (data['two_tailed_95_upper'] > data['true_cases']), True, False)

    validity_68_overall = data.two_tailed_68_valid.sum()/len(data)
    validity_95_overall = data.two_tailed_95_valid.sum()/len(data)
    if verbose:
        print('Overall 68% validity: ', 100*validity_68_overall, '%.')
        print('Overall 95% validity: ', 100*validity_95_overall, '%.')
    
    with open('./results/statistics/' + region_name.rstrip('.csv') + '.txt', 'a') as f:
        f.write(f'Overall 68% validity: {100*validity_68_overall} %.\n')
        f.write(f'Overall 95% validity: {100*validity_95_overall} %.\n')

    # statistics per csps
    table_csps = data[['csps', 'two_tailed_68_valid', 'two_tailed_95_valid']].groupby(['csps']).mean()
    table_csps = table_csps.sort_values(by=['two_tailed_95_valid'], ascending=False)
    tc_string = table_csps.to_string()
    with open('./results/statistics/' + region_name.rstrip('.csv') + '.txt', 'a') as f:
        f.write('\n')
        f.write('Ordered success rate of confidence intervals per csps: \n\n')
        f.write(tc_string)
        f.write('\n')

    # statistics per week
    data['week'] = data.index
    table_week = data[['week', 'two_tailed_68_valid', 'two_tailed_95_valid']].groupby(['week']).mean()
    table_week = table_week.sort_values(by=['two_tailed_95_valid'], ascending=False)
    tw_string = table_week.to_string()
    with open('./results/statistics/' + region_name.rstrip('.csv') + '.txt', 'a') as f:
        f.write('\n')
        f.write('Ordered success rate of confidence intervals per week: \n\n')
        f.write(tw_string)
        f.write('\n')

    data['district'] = region_name.rstrip('.csv')

    return data[['csps', 'week', 'two_tailed_68_valid', 'two_tailed_95_valid', 'district']]

def overall_stats_and_plots(data, plotting = False, verbose = True):
    """
    Perform statistics for all available data across different districts.
    """
    # overall succes rate
    validity_68_overall = data.two_tailed_68_valid.sum()/len(data)
    validity_95_overall = data.two_tailed_95_valid.sum()/len(data)
    if verbose:
        print('\nStatistics across all data:')
        print('Overall 68% validity: ', 100*validity_68_overall, '%.')
        print('Overall 95% validity: ', 100*validity_95_overall, '%.')
    
    with open('./results/statistics/overall.txt', 'w') as f:
        f.write(f'Overall 68% validity: {100*validity_68_overall} %.\n')
        f.write(f'Overall 95% validity: {100*validity_95_overall} %.\n\n')

    # statistics per week
    table_week = data[['week', 'two_tailed_68_valid', 'two_tailed_95_valid']].groupby(['week']).mean()
    table_week = table_week.sort_values(by=['two_tailed_95_valid'], ascending=False)
    tw_string = table_week.to_string()
    with open('./results/statistics/overall.txt', 'a') as f:
        f.write('Ordered success rate of confidence intervals per week: \n\n')
        f.write(tw_string)
        f.write('\n')

    # statistics per district
    def f(x):
        d = {}
        d['two_tailed_68_valid'] = x['two_tailed_68_valid'].mean()
        d['two_tailed_95_valid'] = x['two_tailed_95_valid'].mean()
        d['no_of_csps'] = len(x['csps'].unique())
        return pd.Series(d, index=['two_tailed_68_valid', 'two_tailed_95_valid', 'no_of_csps'])

    table_district = data.groupby('district').apply(f)
    table_district = table_district.sort_values(by=['two_tailed_95_valid'], ascending=False)
    td_string = table_district.to_string()
    
    with open('./results/statistics/overall.txt', 'a') as f:
        f.write('Ordered success rate of confidence intervals per district: \n\n')
        f.write(td_string)
        f.write('\n')
    if plotting:
        # plot of results per district
        no_csps = table_district['no_of_csps'].values.astype(int)
        validity = table_district['two_tailed_95_valid'].values
        plt.rcParams.update({'figure.autolayout': True})
        fig, ax = plt.subplots(figsize=(13, 6))
        ax.barh(table_district.index, 100*table_district['two_tailed_95_valid'])
        labels = ax.get_xticklabels()
        plt.setp(labels, rotation=45)
        ax.axvline(100*validity_95_overall, ls='--', color='r')
        for pos in range(len(table_district.index)):
            ax.text(100*validity[pos]+2, pos, no_csps[pos], fontsize=10,
            verticalalignment="center")
        ax.text(107, 0, 'no of csps per district', fontsize=10,
            verticalalignment="center")
        ax.text(100*validity_95_overall, -3, 'mean', fontsize=10,
            horizontalalignment="center", verticalalignment="center")
        ax.set(xlabel='Validity rate of 95-confidence interval in %', ylabel='District',
        title='Success of prediction per district')
        plt.savefig('./results/png_files/overall_per_district.png', dpi = 500)

        # plot results per week
        fig = plt.figure

def plot_validation_data(available_results, region_name='This region'):
    """
    Plot the results of obtained actual data and computed
    data.

    Returns a plot with subplots for every csps in the 
    corresponding region.
    """
    if len(available_results) == 0:
        print('No data to plot')
        return None
    else:
        pass
    region_name = region_name.rstrip('.csv').rstrip('.xlsx')
    no_rows = int(np.ceil(len(available_results)/2))
    # create subplots for each csps
    fig, axs = plt.subplots(no_rows, ncols=2, figsize=(16,4*no_rows))
    if len(available_results) % 2 != 0:
        if len(available_results) > 1:
            fig.delaxes(axs[-1,-1])
        else:
            fig.delaxes(axs[1])
    plt.subplots_adjust(hspace=0.5)
    # title for the whole figure
    fig.suptitle("Validation vs. computed data for region corresponding to " + region_name,\
         fontsize=15)
    fig.tight_layout(rect=[0, 0.03, 1, 0.95], pad = 5.)

    for i, ax in zip(range(len(available_results)), axs.ravel()):
        values = available_results[i]
        values['true_cases'].plot(marker='o', label = 'true cases', ax=ax)
        ax.fill_between(values.index, values['two_tailed_95_lower'], \
            values['two_tailed_95_upper'], interpolate=True, color='orange', \
                label = 'confidence region 95%')
        ax.fill_between(values.index, values['two_tailed_68_lower'], \
            values['two_tailed_68_upper'], interpolate=True, color='red',  \
                label = 'confidence region 68%')
        ax.set_xlabel('weeks in 2021')
        ax.set_ylabel('number of cases')
        ax.set_title('Malaria cases in ' + values.iat[0,-1])
        ax.legend()
    #plt.subplots_adjust(top=0.5)
    plt.savefig('./results/png_files/' + region_name+'.png', dpi = 500)
    return 0.